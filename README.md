# MacOS Setup

This is a list of steps on how to set up a MacBook for work purposes.

## Settings

First of all, let us look around at the settings. Click on the Apple Logo and chose > System Settings.

### Internet connection

Usually, I add Cloudflare and google DNS to my connection on the advanced settings, IPs:

```dns
1.1.1.1
8.8.8.8
```

### HostName
Go into General > Sharing > Hostname and change it into the \<company name\>.local

### Appearance

Dark mode, with Blue accents and highlights

### Siri & Spotlight

Siri: Off

Spotlight on: Aplications + Calculator + Conversions

### Desktop & Dock

Turn off magnification; Position the dock where it fits best for you.

ON - Minimise windows into application icon;
OFF - Automatic hide and show Dock;
ON - Animate opening applications;
ON - Show indicator for open applications;
OFF - Shoe recent applications on the dock;

### Wallpapers

There is a wallpapers folder on this repo that can be used for this.

### Battery

This is a very important part. There is no guidance here, but the objective is to make it as the computer would stay on forever.

### Lock Screen

Comply with your company rules but keep it less annoying as possible...

### Users and Groups

Here you can pimp your user to look as good as you want on the login page;

### Game Center

Off

### Keyboard

Key repeat rate: Fast

Delay until repeat: Short

### Mouse

Natural scrolling: off

## Finder

Open finder and go into settings:

General tab: New finder window show user folder;
Tags: None
Sidebar: As you like

Create a folder named `Developer` in the user path (`mkdir ~/Developer`).
And a folder named `Projects` in the user path (`mkdir ~/Projects`).

## Developer tools

Before doing anything let's make sure the PC has all the developer tools that it needs to go on:

```sh
xcode-select --install
```

## Homebrew

On this setup, we will be installing a bunch of software and CLI tools. To make it easy to manage later we will do it using the [Homebrew Package Manager](https://brew.sh/).

O install Homebrew follow the steps on: [brew.sh](https://brew.sh/)

## Browser

Whatever fits your workflow at the time.
I suggest firefox:

```sh
brew install --cask firefox
```

Some extensions that might be helpful:
- [ublock-origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
- [grammarly](https://www.grammarly.com/browser/firefox)
- [tampermonkey](https://www.tampermonkey.net/)
    - [YoutubeResizer](https://github.com/Zren/ResizeYoutubePlayerToWindowSize/) 
    - [F1TV-Plus](https://github.com/najdek/f1tv_plus)
    - [Transparent-Twitch-Chat](https://github.com/chylex/Transparent-Twitch-Chat)
    - [twitch-points-auto-claimer](https://greasyfork.org/en/scripts/392348-twitch-auto-channel-points-claimer)

TODO: Config Firefox settings

## Terminal

There are many paths here, the most popular one is [iTerm2](https://iterm2.com/), but, the one that I like the best is [Kitty](https://github.com/kovidgoyal/kitty).
Kitty works great on my machine and that's why I use it. There are some political dramas around the project administration that I'm ignoring for now.

To install kitty run:

```sh
brew install --cask kitty
```

Kitty will store all settings at `.config/kitty/` you can copy my take on those settings at `/kitty` on this repo.

## Shell

Bash is ok, but I prefer ZSH with OhMyZsh.

To install Zsh:

```sh
# Install
brew install zsh

# Enable zsh
chsh -s $(which zsh)

```

Now let's install OhMyZsh by following their install docs here [ohmyz.sh/#install](https://ohmyz.sh/#install).

Now that everything is done, I usually change the settings a bit on the `~/.zshrc` file:

```
export PATH=$HOME/.bin:$PATH

ZSH_THEME="alanpeabody"

zstyle ':omz:update' mode auto

zstyle ':omz:update' frequency 30

HIST_STAMPS="dd/mm/yyyy"

source $HOME/.alias

export EDITOR='nvim'
```

## Alias

As we have sourced the `~/.alias` in the zsh config, we need to create it. Here is an example of what I usually use:

```sh
### Aliases
alias c='clear'
alias top='htop'
alias dev='cd $HOME/Developer'
alias dw='cd $HOME/Downloads'
alias fm='vimfm'

# Docker
alias d='docker'
alias dc='docker-compose'
alias dps='docker ps --format {{.ID}}\\\\t{{.Names}}'

# kubernetes
alias k="kubectl"
alias kns="kubens"
alias kx="kubectx"

# Replace vim
alias vi='nvim'
alias vim='nvim'

# functions                                                                         
function v() {                                                                      
    work_path=`$HOME/.bin/find_work_path`                                           
    cd "$work_path"                                                                 
    nvim .                                                                          
}                                                                                   
                                                                                    
                                                                                    
function vc() {                                                                     
    work_path=`$HOME/.bin/find_work_path`                                           
    cd "$work_path"                                                                 
    code .                                                                          
} 

```

## My scripts

I keep a dir `~/.bin` with all my custom scripts. This dir is part of $PATH.
There is a `/bin` folder in this repo with all my scrits.

## Text editors NeoVim + VSCode

For text editing, I use NeoVim + VSCode depending on the project.

### To install nvim

```sh
brew install neovim
mkdir -p ~/.vim/undodir
```

NeoVim settings should be at `~/.config/nvim`. My current settings are in this repo in the nvim folder, you can just copy and paste in the right dir.

The first time neovim is open you have to run the command `:PackerSync` to install all the plugins.
Optionally, you can comment the `require("user.packer")` at `~/.config/nvim/lua/user/init.lua`, we probably won't need the plugins installer soon, so no need to keep it been loaded every time nvim starts.

I dont usualy install LSP on my neovim but that might be usefull. Check this out: [nvim-lspconfig](https://neovimcraft.com/plugin/neovim/nvim-lspconfig/index.html).

### To install VSCode

```sh
brew install --cask visual-studio-code
```

Cool packages that I like: `tokyo-night`, `Docker`, `shell syntax`, `Terraform`

Cool fonts at: [nerdfonts.com](https://www.nerdfonts.com/font-downloads).

Settings.json

```json
{
    "workbench.colorTheme": "Tokyo Storm Gogh",
    "editor.rulers": [
        80,120
    ],
    "git.confirmSync": false,
    "editor.wordWrap": "on",
    "explorer.confirmDelete": false,
    "redhat.telemetry.enabled": true,
    "editor.unicodeHighlight.ambiguousCharacters": false,
    "explorer.confirmDragAndDrop": false,
    "workbench.sideBar.location": "right",
    "editor.minimap.enabled": false,
    "editor.fontFamily": "'FiraCode Nerd Font', monospace",
    "editor.fontSize": 16,
    "editor.fontWeight": 400,
    "editor.fontLigatures": true,
    "terminal.integrated.fontFamily": "'FiraCode Nerd Font', monospace",
    "terminal.integrated.fontSize": 14,
    "terminal.integrated.fontWeight": "500",
}
```

## SSH keys

I'm assuming some ssh will be required.
To generate a new ssh key pair:

```sh
ssh-keygen -t rsa -b 4096 -f /root/.ssh/id_rsa -N '' < /dev/null
cat /root/.ssh/id_rsa.pub
```

The ssh behavior can be managed at `~/.ssh/config`, mine usually looks something like this:

```sh
# -- IMPORT COMPANY CONFIG -- #
Include company-config-file

# Home server
Host home-server
    HostName 192.168.1.99
    User server

Host anon
    HostName anon.anonserverhost.pt
    User debian

Host aws
    HostName 18.170.40.31
    User ec2-user
    IdentityFile ~/.ssh/personal-aws.pem

```

## GPG

To authenticate my git code I use GPG auth.

To set this up I follow the GitLab documentation for it: [docs.gitlab.com/.../gpg_signed_commits](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/).

## Git

As I use the same computer to review git data for my work and for my personal use I have a common config file and a split section where I can make changes that only affect my work or personal repos. It looks like this:

```sh
[includeIf "gitdir:~/Projects/"]
  path = ~/.gitconfig-personal
[includeIf "gitdir:~/Developer/"]
  path = ~/.gitconfig-work
```

Git stores the configs at `~/.gitconfig`, in this repo you can check the `/git` folder to get my usual configs.

Make sure the `name`, `email`, and `signingkey` are changed in all the files.

To make sure everything is working ok we can do a test:

```sh
cd ~/Projects
mkdir work-test-repo
cd work-test-repo
git init
git config -l
# here we should see the work-related configs
cd .. && rm -rf work-test-repo

# and then do the same in another dir that is not ~/Projects to see if
# other settings are loaded
```

## ViFM

I use `vifm` as a CLI explorer to move around faster.

To install vifm:

```sh
brew install vifm
```

vifm uses `~/.config/vifm/vifmrc` as the config file that you can change to better fit your needs. I like this way:
```
" My settings
set quickview
set dotfiles
set ignorecase

nnoremap qq :q!<cr>
```

## NodeJS
In my opinion, the best way to deal with node is using NVM, and here are the docs on how to install it: [https://github.com/nvm-sh/nvm](https://github.com/nvm-sh/nvm#install--update-script).

After installing NVM restart the shell and let's get node:

To install Node, just run this command in your terminal:

```sh
nvm install node
```

Node is now downloaded but you need to run the following command to actually use it:

```sh
nvm use node
```

Confirm Everything Works with:

```sh
node -v
npm -v
nvm -v
```

## Python3

At the time we should already have python2 and 3 installed on the mac, you can check by:

```sh
python --version
python3 --version
```

Mac comes with python2 for internal use and homebrew installs python3 for it to run.

## Other software

Well, these were my main settings, but, I still use a bunch more software just with the default setups, let's install them all now:

```sh
brew install ansible irssi ripgrep rpl jq kompose kubectx terraform curl mpv tmux nmap fzf wget youtube-dl grep 
```

```sh
brew install --cask telegram mpv tor-browser discord docker font-hack-nerd-font microsoft-teams 1password insomnia keka soda-player spotify tableplus textual transmission typora whatsapp thonny
```

There are some software that is not possible to install with brew and those I usually install manually:
- Magnet for mac: [magnet.crowdcafe.com](https://magnet.crowdcafe.com/)
- Todo App: [ContextSwitcher](https://github.com/mariogarridopt/ContextSwitcher)
- Whiteboard: [Pointless](https://github.com/kkoomen/pointless#installation)
- Kubernetes: [aptakube](https://aptakube.com/)