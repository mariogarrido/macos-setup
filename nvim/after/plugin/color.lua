vim.opt.background = "dark"

require("tokyonight").setup({
    transparent = true,
    on_highlights = function(hl, c)
        local prompt = "#2d3149"
        hl.ColorColumn = {
          bg = "#2B79A0"
        }
    end,
})

vim.cmd("colorscheme tokyonight")