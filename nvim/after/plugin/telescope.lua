vim.cmd(":TSInstall all");

require('telescope').setup{
  defaults = {
    pickers = {
      buffers = {
          theme = "ivy",
      },
      find_files = {
          theme = "dropdown",
      }
    },
    mappings = {
      i = {
        ["<esc>"] = require("telescope.actions").close,
      }
      }
  },
  extensions = {
  }
}