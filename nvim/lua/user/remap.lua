local nnoremap = require("user.keymap").nnoremap

-- remove space actions
nnoremap("<SPACE>", "<Nop>")

-- save and quit
nnoremap("qq", "<cmd>qa!<CR>")
nnoremap("ww", "<cmd>w<CR>")

-- find files
nnoremap("<leader>f", "<cmd>Ex<CR>")
nnoremap("ff", "<cmd>Telescope find_files<CR>")
nnoremap("fg", "<cmd>Telescope live_grep<CR>")

-- Navigate buffers
nnoremap("<leader>n", "<cmd>bnext<CR>")
nnoremap("<leader>p", "<cmd>bprevious<CR>")
nnoremap("<leader>b", "<cmd>Telescope buffers<CR>")
nnoremap("<leader>q", "<cmd>bw<CR>")
